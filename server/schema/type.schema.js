const graphql = require("graphql")

const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLFloat,
} = graphql;

const Person = new GraphQLObjectType({
    name: 'Person',
    description: 'Person Description',
    fields: () => ({
        id: { type: GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLNonNull(GraphQLString) },
        age: { type: GraphQLNonNull(GraphQLInt) },
        isMarried: { type: GraphQLNonNull(GraphQLBoolean) },
        gpa: { type: GraphQLNonNull(GraphQLFloat) },
        justAType: {
            type: Person,
            resolve(parent, args){
                return parent;
            }
        }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'Description',
    fields: {
        person: {
            type: Person,
            resolve(parent,args){
                let personObj = {
                    name: 'Doodii',
                    args: 33,
                    isMarried: true,
                    gpa: 3.99,
                }

                return personObj;
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
});
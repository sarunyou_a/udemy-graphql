const graphql = require('graphql')
var _ = require('lodash')
const User = require('../models/user')
const Hobby = require('../models/hobby')
const Post = require('../models/post')

const {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLID,
    GraphQLNonNull,
} = graphql;

const userData = [
    { id: '1', name: 'Act 1', age: 23, profession: 'programmer' },
    { id: '2', name: 'Act 2', age: 24, profession: 'PO' },
    { id: '3', name: 'Act 3', age: 25, profession: 'SA' },
];

const hobbiesData = [
    { id: '1', title: 'Hobby 1', description: 'desc 1', userId: '2' },
    { id: '2', title: 'Hobby 2', description: 'desc 2', userId: '3' },
    { id: '3', title: 'Hobby 3', description: 'desc 3', userId: '3' },
];

const postData = [
    { id: '1', comment: 'comment 1', userId: '2' },
    { id: '2', comment: 'comment 2', userId: '2' },
    { id: '3', comment: 'comment 3', userId: '1' },
];

const UserType = new GraphQLObjectType({
    name: 'User',
    description: 'Documentation for user...',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        profession: { type: GraphQLString },
        posts: {
            type: new GraphQLList(PostType),
            resolve(parent, args) {
                return Post.find({ userId: parent.id })
            }
        },
        hobbies: {
            type: new GraphQLList(HobbiesType),
            resolve(parent, args) {
                return Hobby.find({ userId: parent.id })
            }
        }
    }),
});

const HobbiesType = new GraphQLObjectType({
    name: 'Hobby',
    description: 'Hobby Description',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        user: {
            type: UserType,
            resolve: (parent, args) => {
                return _.find(userData, { id: parent.userId });
            }
        }
    })
})

const PostType = new GraphQLObjectType({
    name: 'Post',
    description: 'Post Description',
    fields: () => ({
        id: { type: GraphQLID },
        comment: { type: GraphQLString },
        user: {
            type: UserType, resolve: (parent, args) => {
                return _.find(userData, { id: parent.userId });
            }
        },
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'Description',
    fields: {
        user: {
            type: UserType,
            args: {
                id: { type: GraphQLID }
            },
            resolve(parent, args) {
                return User.findById(args.id)
            }
        },
        users: {
            type: GraphQLList(UserType),
            resolve(parent, args) {
                return User.find();
            }
        },
        hobby: {
            type: HobbiesType,
            args: {
                id: { type: GraphQLID },
            },
            resolve(parent, args) {
                return _.find(hobbiesData, { id: args.id });
            }
        },
        hobbies: {
            type: GraphQLList(HobbiesType),
            resolve(parent, args) {
                return hobbiesData;
            }
        },
        post: {
            type: PostType,
            args: {
                id: { type: GraphQLID },
            },
            resolve(parent, args) {
                return _.find(postData, { id: args.id });
            }
        },
        posts: {
            type: GraphQLList(PostType),
            resolve(parent, args) {
                return postData;
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createUser: {
            type: UserType,
            args: {
                name: { type: GraphQLNonNull(GraphQLString) },
                age: { type: GraphQLInt },
                profession: { type: GraphQLString },
            },
            resolve(parent, args) {
                let user = User({
                    name: args.name,
                    age: args.age,
                    profession: args.profession,
                })

                return user.save()
            }
        },
        updateUser: {
            type: UserType,
            args: {
                id: { type: graphql.GraphQLNonNull(GraphQLString) },
                name: { type: GraphQLString },
                age: { type: GraphQLInt },
                profession: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateUser = User.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            name: args.name,
                            age: args.age,
                            profession: args.profession,
                        },
                    },
                    { new: true }
                )
            }
        },
        removeUser: {
            type: UserType,
            args: {
                id: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                let removeUser = User.findByIdAndDelete(args.id)

                if (!removeUser) {
                    throw 'can not delete this id ' + args.id;
                }

                return removeUser
            }
        },
        createPost: {
            type: PostType,
            args: {
                comment: { type: GraphQLNonNull(GraphQLString) },
                userId: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                let post = Post({
                    comment: args.comment,
                    userId: args.userId,
                })

                return post.save()
            }
        },
        updatePost: {
            type: PostType,
            args: {
                id: { type: GraphQLNonNull(GraphQLString) },
                comment: { type: GraphQLNonNull(GraphQLString) },
                userId: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updatePost = Post.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            comment: args.comment,
                            userId: args.userId,
                        }
                    },
                    { new: true }
                )
            }
        },
        removePost: {
            type: PostType,
            args: {
                id: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                let removePost = Post.findByIdAndDelete(args.id)

                if (!removePost) {
                    throw 'can not delete this id ' + args.id;
                }

                return removePost
            }
        },
        createHobby: {
            type: HobbiesType,
            args: {
                title: { type: GraphQLNonNull(GraphQLString) },
                description: { type: GraphQLString },
                userId: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                let hobby = Hobby({
                    title: args.title,
                    description: args.description,
                    userId: args.userId,
                })

                return hobby.save()
            }
        },
        updateHobby: {
            type: HobbiesType,
            args: {
                id: { type: GraphQLNonNull(GraphQLString) },
                title: { type: GraphQLNonNull(GraphQLString) },
                description: { type: GraphQLString },
                userId: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateHobby = Hobby.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            title: args.title,
                            description: args.description,
                            userId: args.userId,
                        }
                    },
                    { new: true }
                );
            }
        },
        removeHobby: {
            type: HobbiesType,
            args: {
                id: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                let removeHobby = Hobby.findByIdAndDelete(args.id)

                if (!removeHobby) {
                    throw 'can not delete this id ' + args.id;
                }

                return removeHobby
            }
        },
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});
const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const { URLSearchParams } = require('url');
const {mongoose} = require('mongoose');
const schema = require('./schema/schema');
const typeSchema = require('./schema/type.schema');
const cors = require('cors')

global.URLSearchParams = URLSearchParams;

const app = express();
const port = process.env.PORT || 4000;

app.use(cors())

app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema: schema,
}));

app.use('/types', graphqlHTTP({
    graphiql: true,
    schema: typeSchema,
}));

mongoose.connect(
    `mongodb+srv://${process.env.mongoUserName}:${process.env.mongoUserPassword}@graphql.dtxpwu4.mongodb.net/?retryWrites=true&w=majority`,
).then(() => {
    app.listen({port: port}, () => {
        console.log('Start Server Port : ' + port);
    }); 
}).catch((error) => {
    console.log(`MongoDB Error : ${error}`)
});


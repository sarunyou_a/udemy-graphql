import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController controller = TextEditingController();
  double fieldWidth = 250;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Demo GraphQL'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Row(
              children: [
                const Spacer(),
                AnimatedContainer(
                  duration: const Duration(milliseconds: 800),
                  width: fieldWidth,
                  child: TextField(
                    controller: controller,
                    style: const TextStyle(fontSize: 16),
                    decoration: const InputDecoration(
                      hintText: 'ชื่อ,อายุ,ตำแหน่ง',
                      isDense: true,
                      contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                  ),
                ),
                const SizedBox(width: 12),
                Mutation(
                  options: MutationOptions(
                    document: gql('''
mutation CreateUser(\$name: String!, \$age: Int!, \$pro : String!){
  createUser(name: \$name, age: \$age, profession: \$pro){
    id
  }
}
'''),
                    onCompleted: (data) {
                      print('complete : $data');
                      setState(() {});
                    },
                    onError: (error) {
                      print('error $error');
                    },
                  ),
                  builder: (runMutation, result) {
                    return ElevatedButton(
                      onPressed: () {
                        final data = controller.text.split(',');

                        final name = data.isNotEmpty ? data[0].trim() : null;
                        final age = data.length > 1 ? data[1].trim() : null;
                        final pro = data.length > 2 ? data[2].trim() : null;

                        if (name == null || name.isEmpty || age == null || age.isEmpty || pro == null || pro.isEmpty) {
                          return;
                        }

                        runMutation({
                          'name': name,
                          'age': int.parse(age),
                          'pro': pro,
                        });

                        controller.clear();
                      },
                      child: const Text('เพิ่ม User'),
                    );
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: Query(
              builder: (QueryResult result, {VoidCallback? refetch, FetchMore? fetchMore}) {
                final length = result.data?['users'] != null ? (result.data?['users'] as List).length : 0;
                return ListView.separated(
                  itemCount: length,
                  itemBuilder: (context, index) {
                    final data = result.data!['users'][index] as Map<String, dynamic>;
                    return ListTile(
                      minVerticalPadding: 10,
                      title: Text(data['name']),
                      subtitle: Row(
                        children: [
                          Text("อายุ ${data['age'].toString()} ปี"),
                          Text(" : ${data['profession']}"),
                        ],
                      ),
                      trailing: Mutation(
                        options: MutationOptions(
                          document: gql('''
mutation RemoveUser(\$id: String!){
  removeUser(id: \$id){
    id
  }
}
'''),
                          onCompleted: (data) => setState(() {}),
                          onError: (error) => print('error: $error'),
                        ),
                        builder: (runMutation, result) {
                          return TextButton(
                            onPressed: () {
                              runMutation({'id': data['id']});
                            },
                            child: const Text('ลบ'),
                          );
                        },
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => const Divider(),
                );
              },
              options: QueryOptions(
                document: gql('''
query{
  users{
    id
    name
    age
    profession
  }
}
      '''),
                variables: const {
                  'nRepositories': 50,
                },
                pollInterval: const Duration(seconds: 10),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
